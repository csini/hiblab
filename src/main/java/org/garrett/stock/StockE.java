package org.garrett.stock;

import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by 70036 on 14/12/2017.
 */
@Entity
@Table(name = "stock", catalog = "stockdb", uniqueConstraints = {
        @UniqueConstraint(columnNames = "STOCK_NAME"),
        @UniqueConstraint(columnNames = "STOCK_CODE") })
public class StockE extends Stock implements java.io.Serializable {
        

    public StockE(String code, String name) {
		super(code,name);
	}

	public StockE() {
		super();
	}

	@Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if (this == o) return true;
        if (!(o instanceof StockE)) return false;
        System.out.println("OVERRIDED EQUALS");
        StockE that = (StockE) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.getStockCode(),that.getStockCode())
                .append(this.getStockName(),that.getStockName());
        return eb.isEquals();
    }

    @Override
    public int hashCode() {
        System.out.println("OVERRIDED HASHCODE");

        return Objects.hash(getStockCode(),getStockName());

    }
}
