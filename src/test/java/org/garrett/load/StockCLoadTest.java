package org.garrett.load;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import org.garrett.stock.StockC;
import org.garrett.stock.StockDailyRecordC;
import org.garrett.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.ThrowingConsumer;

//@Disabled
public class StockCLoadTest {
	private int cycles = 10000;
	private static SessionFactory sf = null;
	private static Session session;
	int counter = 1;
	int totTime = 0;

	public static void initDB() {
		sf = HibernateUtil.getSessionFactory();
		session = sf.openSession();
	}

	@TestFactory
	public Stream<DynamicTest> insertStocksC() {
		int code = 7052;
		String name = "PADINI";
		Iterator<StockC> inputGenerator = new Iterator<StockC>() {

			@Override
			public boolean hasNext() {
				return counter % cycles != 0;
			}

			public StockC next() {
				int sfx = counter++;
				StockC stock = new StockC(String.format("%d_%07d", code, sfx), (String.format("PADINI_%07d", sfx)));

				StockDailyRecordC stockDailyRecords = new StockDailyRecordC();
				stockDailyRecords.setPriceOpen(new Float("1.2"));
				stockDailyRecords.setPriceClose(new Float("1.1"));
				stockDailyRecords.setPriceChange(new Float("10.0"));
				stockDailyRecords.setVolume(3000000L);
				stockDailyRecords.setDate(new Date());

				stockDailyRecords.setStock(stock);
				stock.getStockDailyRecords().add(stockDailyRecords);

				return stock;
			}
		};

		// Generates display names like: input:5, input:37, input:85, etc.
		Function<StockC, String> displayNameGenerator = (input) -> {
			String result = String.format("SS insserting %s ", input.toString());
			System.out.println(result);
			return result;
		};

		ThrowingConsumer<StockC> testExecutor = (input1) -> {

			// session = HibernateUtil.getSessionFactory().getCurrentSession();
			if (session == null) {
				session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
			}
			long start = System.currentTimeMillis();

			session.save(input1);
			session.flush();
			long end = System.currentTimeMillis();

			System.out.printf("Tempo impiegto %d \n", end - start);
			totTime += (end - start);
			System.out.printf("###############################%d###########################\n", totTime);

		};
		// Returns a stream of dynamic tests.
		return DynamicTest.stream(inputGenerator, displayNameGenerator, testExecutor);
	}

	public static void queryDB() {
		Criteria cri = session.createCriteria(StockC.class);
		long start = System.currentTimeMillis();
		List<StockC> lista = cri.list();
		System.out.printf("Result Size %d \n", lista.size());
		long end = System.currentTimeMillis();
		System.out.printf("Tempo impiegto %d \n", end - start);
		start = System.currentTimeMillis();
		for (StockC s : lista) {
			String.format("%s", s);
		}
		end = System.currentTimeMillis();
		System.out.printf("Tempo impiegto sorrere lista %d \n", end - start);
	}

	@AfterAll
	public static void end() {
		queryDB();
		if (session != null)
			session.close();
	}
}
