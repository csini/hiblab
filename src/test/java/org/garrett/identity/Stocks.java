package org.garrett.identity;

import org.garrett.stock.Stock;

class Stocks{
    Stock stock;
    Stock stock1;
    String descrizione;

    public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Stocks(String descrizione) {
		super();
		this.descrizione = descrizione;
	}

	public Stocks() {
		super();
		
	}

	public Stocks(Stock stock, Stock stock1) {
        this.stock = stock;
        this.stock1 = stock1;
    }

    public void setStock(Stock stock) {
		this.stock = stock;
	}

	public void setStock1(Stock stock1) {
		this.stock1 = stock1;
	}

	public Stock getStock() {
        return stock;
    }

    public Stock getStock1() {
        return stock1;
    }
}