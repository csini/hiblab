package org.garrett.identity;

import java.util.List;
import java.util.function.Function;

import org.garrett.IdentityTests;
import org.garrett.stock.Stock;
import org.garrett.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Manage of the context on which objects to be compared get created.
 * @author 70036
 *
 */
public class IdentityContext {

	public Function<IdentityTests, Stocks> buildStocks = (input) -> {
		Stocks stck = null;
		
		switch(input) {
		
		case STOCK_FILLED_DB:
			stck = loadStocksFromAHibSession();
			break;
		default:
			stck = buildEmptyStocks();
		}
		
		return stck;
	};
	
	public Function<IdentityTests, Stocks> getBuildStocks() {
		return buildStocks;
	}

	/**
	 * 2 instances from the same hibernate session
	 * @return
	 */
	Stocks loadStocksFromAHibSession() {
    	System.out.println("Hibernate one to many (Annotation)");
    	Stocks stocks = new Stocks();
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7052");
        stock.setStockName("PADINI");
        Criteria crit = session.createCriteria(Stock.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        List<Stock> results = crit.list();
        stocks.setStock(results.get(0));       		
        
        Stock stock1 = new Stock();
        stock1.setStockCode("7052");
        stock1.setStockName("PADINI");
        crit = session.createCriteria(Stock.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        results = crit.list();
        stocks.setStock1(results.get(0));       		
        
        session.close();
        
        return stocks;
    }
	
	/**
	 * Build empty stocks in a java context
	 * @return
	 */
	Stocks buildEmptyStocks() {
		Stocks st = new Stocks(new Stock(), new Stock());
		st.setDescrizione("Empty stocks without equal contract implemented");		
		
		return st;
		
	}
	
}
