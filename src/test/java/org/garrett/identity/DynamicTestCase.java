package org.garrett.identity;

import org.garrett.App;
import org.garrett.IdentityTests;
import org.garrett.stock.Stock;
import org.garrett.stock.StockE;
import org.garrett.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.ThrowingConsumer;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;


/**
 * Created by 70036 on 15/12/2017.
 */
@Disabled
public class DynamicTestCase {

    
    int counter = 0;
    IdentityTests test = IdentityTests.STOCK_EMPTY;

    @BeforeAll
    public static void init() {
		App.initDB(null);
	}
	
    
    
    //@TestFactory
    Stream<DynamicTest> dynamicTestsFromStream() {
        return Stream.of("A", "B", "C")
            .map(str -> dynamicTest("test" + str, () -> { /* ... */ }));
    }

    
    @TestFactory
    public Stream<DynamicTest> generateRandomNumberOfTests() {

    	// Generates random positive integers between 0 and 100 until
        // a number evenly divisible by 7 is encountered.
        Iterator<Stocks> inputGenerator = new Iterator<Stocks>() {

            
            Stocks current;

            @Override
            public boolean hasNext() {
            	System.out.println("hasNext");
                switch (test) {
                    case STOCK_EMPTY:
                    	System.out.println("STOCK_EMPTY");
                        current = new Stocks(new Stock(), new Stock());
                        test = IdentityTests.STOCK_FILLED;
                        break;
                    case STOCK_FILLED:
                    	System.out.println("STOCK_FILLED");
                        current = new Stocks(new Stock("pippo","pippone"), new Stock("pippo","pippone"));
                        test = IdentityTests.STOCK_FILLED_EQUUALIZED;
                        break;
                    case STOCK_FILLED_EQUUALIZED:
                    	System.out.println("STOCK_FILLED_EQUUALIZED");
                        current = new Stocks(new StockE("pippo","pippone"), new StockE("pippo","pippone"));
                        test = IdentityTests.STOCK_FILLED_DB;
                        break;
                    case STOCK_FILLED_DB:
                    	System.out.println("STOCK_FILLED_DB");
                        current = loadStockEntity();
                        test = IdentityTests.STOCK_FILLED_DB_2_SESSIONS;
                        break;
                    case STOCK_FILLED_DB_2_SESSIONS:
                    	System.out.println("STOCK_FILLED_DB_2_SESSIONS");
                        current = loadStockEntity2Session();
                        test = IdentityTests.DEFAULT;
                    	break;
                    case DEFAULT:
                        test = IdentityTests.DEFAULT;
                        current = null;
                        break;
                }

                return current != null;
            }

            public Stocks next() {
            	System.out.println("next");
                return current;
            }
        };

        // Generates display names like: input:5, input:37, input:85, etc.
        Function<Stocks, String> displayNameGenerator = (input) -> {
        	String result = String.format("SS matching %s To %s",input.getStock().toString(),input.getStock1().toString());
        	System.out.println(result);
        	return result;
        };


        // Generates display names like: input:5, input:37, input:85, etc.
        ThrowingConsumer<Stocks> testExecutor = (input1) -> {
        	System.out.printf("chekEquality %s -- %s \n", System.identityHashCode(input1.getStock()), System.identityHashCode(input1.getStock1()));
        	if(input1.getStock() == input1.getStock1())
        		System.out.println("SAME");
        	else
        		System.out.println("NOT SAME!");
        	System.out.println("#######Filling Set#############");
        	Set<Stock> dep = new LinkedHashSet<Stock>();
        	dep.add(input1.getStock());
        	dep.add(input1.getStock1());
        	System.out.println("Set size: " + dep.size());
        	System.out.println("##########################################################");
        	
           
        };
        // Returns a stream of dynamic tests.
        return DynamicTest.stream(inputGenerator, displayNameGenerator, testExecutor);
    }
    
    
    
    private Stocks loadStockEntity() {
    	System.out.println("Hibernate one to many (Annotation)");
    	Stocks stocks = new Stocks();
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7052");
        stock.setStockName("PADINI");
        Criteria crit = session.createCriteria(Stock.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        List<Stock> results = crit.list();
        stocks.setStock(results.get(0));       		
        
        Stock stock1 = new Stock();
        stock1.setStockCode("7052");
        stock1.setStockName("PADINI");
        crit = session.createCriteria(Stock.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        results = crit.list();
        stocks.setStock1(results.get(0));       		
        
        session.close();
        
        return stocks;
    }
    
    private Stocks loadStockEntity2Session() {
    	System.out.println("Hibernate one to many (Annotation)");
    	Stocks stocks = new Stocks();
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7052");
        stock.setStockName("PADINI");
        Criteria crit = session.createCriteria(Stock.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        List<Stock> results = crit.list();
        stocks.setStock(results.get(0));       		
        session.close();
        
        session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
        Stock stock1 = new Stock();
        stock1.setStockCode("7052");
        stock1.setStockName("PADINI");
        crit = session.createCriteria(Stock.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        results = crit.list();
        stocks.setStock1(results.get(0));       		
        
        session.close();
        
        return stocks;
    }
    
    /**
     * Due istanze ottenute da 2 differenti sessioni hibernate
     * @return
     */
    private Stocks loadStockEEntity2Session() {
    	System.out.println("Hibernate one to many (Annotation)");
    	Stocks stocks = new Stocks();
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		StockE stock = new StockE();
        stock.setStockCode("7052");
        stock.setStockName("PADINI");
        Criteria crit = session.createCriteria(StockE.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        List<Stock> results = crit.list();
        stocks.setStock(results.get(0));       		
        session.close();
        
        session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
        StockE stock1 = new StockE();
        stock1.setStockCode("7052");
        stock1.setStockName("PADINI");
        crit = session.createCriteria(StockE.class);
        crit.add(Restrictions.eq("stockCode","7052"));
        crit.add(Restrictions.eq("stockName","PADINI"));
        results = crit.list();
        stocks.setStock1(results.get(0));       		
        
        session.close();
        
        return stocks;
    }
}
