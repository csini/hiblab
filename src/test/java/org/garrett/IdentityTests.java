package org.garrett;

public enum IdentityTests {
	DEFAULT,
	STOCK_EMPTY,
	STOCK_FILLED,
	STOCK_FILLED_EQUUALIZED,
	STOCK_FILLED_DB,
	STOCK_FILLED_DB_2_SESSIONS
}
