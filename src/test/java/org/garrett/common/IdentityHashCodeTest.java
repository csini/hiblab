package org.garrett.common;

import org.garrett.stock.Stock;
import org.garrett.stock.StockE;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by 70036 on 14/12/2017.
 */

public class IdentityHashCodeTest {

    @Test
    public void identityTest(){


        StockE stockE = new StockE();
        StockE stock2E = new StockE();

        System.out.printf("Istanze %s / %s", System.identityHashCode(stockE),System.identityHashCode(stock2E));
        if(stockE == stock2E)
          log("sono ugual!" );


        stockE = new StockE("pippo", "pippo");
        stock2E = new StockE("pippo", "pippo");
        log("Identity -->SSTART");
        System.out.printf("\nIstanze %s / %s", System.identityHashCode(stockE),System.identityHashCode(stock2E));
        log("Identity -->END");
        System.out.printf("\nIstanze %s / %s", stockE.hashCode(),stock2E.hashCode());
        System.out.printf("\nIstanze equals %s ", stockE.equals(stock2E));
        if(stockE == stock2E)
            log("\nidentiche" );
        else
            log("\ndiverse" );

        Set<StockE> s = new HashSet<StockE>();
        s.add(stockE);
        s.add(stock2E);
        s.size();
        System.out.printf("\nIstanze size %s ",s.size());
    }

    @Test
    public void withoutHE(){
        Stock stock1 = new Stock();
        Stock stock2 = new Stock();

        Assert.assertNotSame(stock1,stock2);
        log("istanze " + System.identityHashCode(stock1) + " / " + System.identityHashCode(stock2) + " sono differenti!" );

    }

    private void log(String message){
         System.out.println(message);
    }


}